﻿using DeliveryConfirmation.Enums;
using DeliveryConfirmation.Models;
using DeliveryConfirmation.Repositories;
using Microsoft.AspNet.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryConfirmation.Controllers.Api
{
    public class ShipmentController : Controller
    {
        public static ShipmentRepository _repository;
        public ShipmentController()
        {
            if(_repository == null)
                _repository = new ShipmentRepository();
        }

        [HttpGet("api/shipments")]
        public JsonResult Get(int startingNumber)
        {
            var results = _repository.GetShipments(startingNumber);
            var total = _repository.Count();
            var response = new Dictionary<string, object>()
            {
                { "shipments" , results },
                { "total" , total },

            };
            return Json(response);
        }

        [HttpPut("api/shipments")]
        public JsonResult Put([FromBody]Shipment shipment)
        {
            _repository.UpdateShipment(shipment);
            return Json(null);
        }

    }
}
