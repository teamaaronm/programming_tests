﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryConfirmation.Enums
{
    public enum PackageStatus
    {
        None,
        UnDamaged,
        Damaged
    }
}
