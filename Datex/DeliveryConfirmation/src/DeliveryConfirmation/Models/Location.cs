﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryConfirmation.Models
{
    public class Location
    {
        public string Address { get; set; }
        public string Name { get; set; }
    }
}
