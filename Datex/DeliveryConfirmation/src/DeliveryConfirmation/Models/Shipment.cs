﻿using DeliveryConfirmation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryConfirmation.Models
{
    public class Shipment
    {
        public Location Origin { get; set; }
        public Location Destination { get; set; }
        public int NumberOfPackages { get; set; }
        public string TrackingNumber { get; set; }
        public ShipmentStatus Status { get; set; }
        public PackageStatus PackageStatus { get; set; }
        public CustomerStatus CustomerStatus { get; set; }
        public string Notes { get; set; }
    }
}
