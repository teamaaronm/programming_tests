﻿using DeliveryConfirmation.Enums;
using DeliveryConfirmation.Models;
using EfficientlyLazy.IdentityGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryConfirmation.Repositories
{
    public class ShipmentRepository
    {
        private List<Shipment> _shipments;
        public ShipmentRepository()
        {
            var random = new Random();
            _shipments = new List<Shipment>();
            for (int i = 0; i < 50; i++)
            {
                var originAddressGen = Generator.GenerateAddress();
                var originNameGen = Generator.GenerateName();
                var destAddressGen = Generator.GenerateAddress();
                var destNameGen = Generator.GenerateName();
                var trackingNumberGen = Generator.GenerateSSN();

                var shipment = new Shipment()
                {
                    Origin = new Location()
                    {
                        Name = string.Format("{0} {1}", originNameGen.First, destNameGen.Last),
                        Address = string.Format("{0} {1} {2} {3}", originAddressGen.AddressLine, originAddressGen.City, originAddressGen.StateAbbreviation, originAddressGen.ZipCode)
                    },
                    Destination = new Location()
                    {
                        Name = string.Format("{0} {1}", destNameGen.First, destNameGen.Last),
                        Address = string.Format("{0} {1} {2} {3}", destAddressGen.AddressLine, destAddressGen.City, destAddressGen.StateAbbreviation, destAddressGen.ZipCode)
                    },
                    NumberOfPackages = random.Next(0, 15),
                    Status = (ShipmentStatus)Enum.Parse(typeof(ShipmentStatus), random.Next(0, 3).ToString()),
                    TrackingNumber = trackingNumberGen,
                    CustomerStatus = (CustomerStatus)Enum.Parse(typeof(CustomerStatus), random.Next(1, 3).ToString()),
                    PackageStatus = (PackageStatus)Enum.Parse(typeof(PackageStatus), random.Next(1, 3).ToString()),
                    Notes = random.Next(0, 500000).ToString()
                };

                if (shipment.Status == ShipmentStatus.OutForDelivery)
                {
                    shipment.CustomerStatus = CustomerStatus.None;
                    shipment.PackageStatus = PackageStatus.None;
                    shipment.Notes = null;
                }

                _shipments.Add(shipment);
            }
        }
        public List<Shipment> GetShipments(int startingNumber)
        {
            return _shipments.GetRange(startingNumber, 25);
        }

        public void UpdateShipment(Shipment shipment)
        {
            var index = _shipments.FindIndex(x => x.TrackingNumber.Equals(shipment.TrackingNumber));
            _shipments[index] = shipment;
        }

        public int Count()
        {
            return _shipments.Count;
        }
    }
}
