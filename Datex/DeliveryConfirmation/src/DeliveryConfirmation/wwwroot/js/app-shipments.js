﻿(function () {
    "use strict";
    angular.module("app-shipments", ["ngRoute", "bw.paging"])
      .config(function ($routeProvider) {
          if (isMobile.apple.phone || isMobile.android.phone || isMobile.seven_inch) {
              $routeProvider.when("/", {
                  templateUrl: "/views/shipmentsMobile.html"
              });
          }
          else {
              $routeProvider.when("/", {
                  templateUrl: "/views/shipments.html"
              });
          }
          $routeProvider.otherwise({ redirectTo: "/" });
      }).constant('shipmentStatus', [
            { name: "Out-For-Delivery", id: 0 },
            { name: "Delivered", id: 1 },
            { name: "Held-In-Truck", id: 2 }
      ]).service('shipmentService', function ($http) {
          var shipments = [];
          var total = 0;
          return {
              /*
               * Get shipments from the server shipment list starting at startingIndex.
               */
              getShipments: function (startingIndex, callback) {
                  $http.get("/api/shipments", {
                      params: { startingNumber: startingIndex }
                  })
                      .then(function (response) {
                          angular.copy(response.data.shipments, shipments);
                          total = response.data.total;
                          callback(shipments, total);
                      }, function (error) {
                          // Failure
                          vm.errorMessage = "Failed to load data: " + error;
                      })
                      .finally(function () {
                          vm.isBusy = false;
                      });
              },
              /*
               * Get the shipment that has tracking number trackingNumber.
               */
              getShipment: function (trackingNumber, callback) {
                  shipments.forEach(function (shipment) {
                      if (shipment.trackingNumber == trackingNumber) {
                          callback(shipment);
                          return;
                      }
                  });
              },
              /*
               * Update a shipment.
               */
              updateShipment: function (shipment, callback) {
                  shipments.forEach(function (s) {
                      if (s.trackingNumber == shipment.trackingNumber) {
                          for (var attr in shipment) {
                              if (shipment.hasOwnProperty(attr))
                                  s[attr] = shipment[attr];
                          }
                      }
                  });
                  $http.put("/api/shipments", shipment)
                      .then(function (response) {
                          callback();
                      }, function (error) {
                          // Failure
                          vm.errorMessage = "Failed to load data: " + error;
                      })
                      .finally(function () {
                          vm.isBusy = false;
                      });
              }
          };
      }).service('eventManager', function () {
          var listeners = {};
          return {
              register: function (name, callback) {
                  if (listeners[name] == null)
                      listeners[name] = [];
                  listeners[name].push(callback);
              },
              emit: function (name, parameters) {
                  if (listeners[name] != null) {
                      listeners[name].forEach(function (listener) {
                          listener(parameters);
                      });
                  }
              }
          };
      });
})();