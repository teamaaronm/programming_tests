﻿(function () {

    "use strict";

    angular.module("app-shipments")
      .controller("mobileController", mobileController);

    function mobileController($http, $location, shipmentService, eventManager) {
        var vm = this;
        vm.isVisible = false;
        eventManager.register("enterDetails", function (parameters) {
            vm.isVisible = true;
            eventManager.emit("showShipmentList", false);
            eventManager.emit("showShipmentDetails", true);
        });

        vm.back = function (shipment) {
            vm.isVisible = false;
            eventManager.emit("showShipmentList", true);
            eventManager.emit("showShipmentDetails", false);
        }
    }

})();