﻿(function () {
    "use strict";
    angular.module("app-shipments")
      .controller("shipmentDetailsController", shipmentDetailsController);
    function shipmentDetailsController($http, $location, shipmentService, eventManager, shipmentStatus) {
        var vm = this;
        vm.isSelected = false;
        vm.error = "none";
        vm.shipments = [];
        vm.isVisible = false;
        eventManager.register("enterDetails", function (trackingNumber) {
            shipmentService.getShipment(trackingNumber, function (shipment) {
                vm.shipment = shipment;
                vm.shipment.statusString = shipmentStatus[vm.shipment.status].name;
            });
        });
        vm.deliver = function () {
            if (vm.customerStatus == 0) {
                vm.error = "required";
            }
            else {
                vm.error = "none";
                var status = shipmentStatus.filter(function (status) { return status.name == "Delivered"; });
                vm.shipment.status = status[0].id;
                vm.shipment.statusString = status[0].name;
                shipmentService.updateShipment(vm.shipment, function () {
                    eventManager.emit("updateList");
                });
            }
        }
        vm.hold = function () {
            if (vm.shipment.notes == null) {
                vm.error = "notesRequired";
            }
            else
            {
                vm.error = "none";
                var status = shipmentStatus.filter(function (status) { return status.name == "Held-In-Truck"; });
                vm.shipment.statusString = status[0].name;
                vm.shipment.status = status[0].id;
                shipmentService.updateShipment(vm.shipment, function () {
                    eventManager.emit("updateList");
                });
            }
        }
        vm.undo = function () {
            var status = shipmentStatus.filter(function (status) { return status.name == "Out-For-Delivery"; })[0];
            vm.shipment.status = status.id;
            vm.shipment.statusString = status.name;
            shipmentService.updateShipment(vm.shipment, function () {
                eventManager.emit("updateList");
            });
        }
        eventManager.register("showShipmentDetails", function (parameters) {
            vm.isVisible = parameters;
        });
    }

})();