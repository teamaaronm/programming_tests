﻿(function () {

    "use strict";

    angular.module("app-shipments")
      .controller("shipmentListController", shipmentListController);

    function shipmentListController($http, $location, shipmentService, eventManager) {

        var vm = this;

        vm.errorMessage = "";
        vm.isVisible = true;

        eventManager.register("updateList", function () {
            vm.filter(vm.selectedFilter);
        });

        vm.selectShipment = function (shipment) {
            eventManager.emit("enterDetails", shipment.trackingNumber);
        }

        vm.changePage = function (page) {
            shipmentService.getShipments(25 * (page - 1), function (shipments, total) {
                vm.shipments = shipments;
                vm.total = total;
                vm.page = page;
            });
        };

        vm.filter = function (filter) {
            vm.selectedFilter = filter;
            if (filter == 'All') {
                vm.shipments = jQuery.grep(vm.shipments, function (shipment) {
                    shipmentService.getShipments(vm.page, function (shipments, total) {
                        vm.shipments = shipments;
                        vm.total = total;
                    });
                });
            }
            else if (filter == 'Delivered') {
                shipmentService.getShipments(vm.page, function (shipments, total) {
                    vm.shipments = jQuery.grep(shipments, function (shipment) {
                        if (shipment.status == 1)
                            return true;
                    });
                    vm.total = vm.shipments.count;
                });

            }
            else if (filter == 'Out-For-Delivery') {
                shipmentService.getShipments(vm.page, function (shipments, total) {
                    vm.shipments = jQuery.grep(shipments, function (shipment) {
                        if (shipment.status == 0)
                            return true;
                    });
                    vm.total = vm.shipments.count;
                });
            }
            else if (filter == 'Held-In-Truck') {
                shipmentService.getShipments(vm.page, function (shipments, total) {
                    vm.shipments = jQuery.grep(shipments, function (shipment) {
                        if (shipment.status == 2)
                            return true;
                    });
                    vm.total = vm.shipments.count;
                });
            }
        }

        eventManager.register("showShipmentList", function (parameters) {
            vm.isVisible = parameters;
        });

        vm.filter("Out-For-Delivery");

    }

})();