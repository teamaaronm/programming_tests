﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitManipulation
{
    class FlippingBits
    {
        static void Main(String[] args)
        {
            var size = Convert.ToInt32(Console.ReadLine());
            var array = new string[size];
            for(int i = 0; i < size; i++)
            {
                var number = Convert.ToUInt32(Console.ReadLine());
                array[i] = (~number).ToString();
                Console.WriteLine(array[i]);
            }
        }
    }
}
