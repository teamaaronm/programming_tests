﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicProgramming
{
    public class MrKmarsh
    {
        public static int GetLargestPeremeter(int height, int width, char[][] cases, Dictionary<Tuple<int, int>, bool> swamps = null)
        {
            var largestPeremeter = 0;
            if (swamps == null)
            {
                swamps = new Dictionary<Tuple<int, int>, bool>();
                for (int i = 0; i < height; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        if (cases[i][j].Equals('x'))
                            swamps.Add(new Tuple<int, int>(i, j), true);
                    }
                }
            }
            for (int topLeft_x = 0; topLeft_x < width; topLeft_x++)
            {
                for (int topLeft_y = 0; topLeft_y < height; topLeft_y++)
                {
                    if (swamps.ContainsKey(new Tuple<int, int>(topLeft_y, topLeft_x)))
                        continue;
                    for (int bottomRight_x = 0; bottomRight_x < width; bottomRight_x++)
                    {
                        for (int bottomRight_y = 0; bottomRight_y < height; bottomRight_y++)
                        {
                            if (swamps.ContainsKey(new Tuple<int, int>(bottomRight_y, bottomRight_x)))
                                continue;
                            largestPeremeter = Math.Max(largestPeremeter, Peremeter(topLeft_x, topLeft_y, bottomRight_x, bottomRight_y, cases, swamps));
                        }
                    }
                }
            }

            return largestPeremeter;
        }

        static void Main(String[] args)
        {
            var dimensions = Console.ReadLine().Split(' ');
            var height = Convert.ToInt32(dimensions[0]);
            var width = Convert.ToInt32(dimensions[1]);
            var cases = new char[height][];
            var largestPeremeter = 0;
            var swamps = new Dictionary<Tuple<int, int>, bool>();
            for (int i = 0; i < height; i++)
            {
                var line = Console.ReadLine();
                cases[i] = new char[width];
                for (int j = 0; j < width; j++)
                {
                    cases[i][j] = line[j];
                    if (line[j].Equals('x'))
                        swamps.Add(new Tuple<int, int>(i, j), true);
                }
            }

            largestPeremeter = GetLargestPeremeter(height, width, cases, swamps);

            if (largestPeremeter != 0)
                Console.WriteLine(largestPeremeter);
            else
                Console.WriteLine("impossible");
        }

        static int Peremeter(int topLeft_x, int topLeft_y, int bottomRight_x, int bottomRight_y, char[][] cases, Dictionary<Tuple<int, int>, bool> swamps)
        {
            if (bottomRight_x - topLeft_x < 1)
                return 0;
            else if (bottomRight_y - topLeft_y < 1)
                return 0;
            foreach (var swamp in swamps.Keys)
            {
                if (swamp.Item1.Equals(topLeft_y) && bottomRight_x - swamp.Item2 >= 0 && swamp.Item2 - topLeft_x >= 0)
                    return 0;
                else if (swamp.Item1.Equals(bottomRight_y) && bottomRight_x - swamp.Item2 >= 0 && swamp.Item2 - topLeft_x >= 0)
                    return 0;
                else if (bottomRight_y - swamp.Item1 >= 0 && swamp.Item1 - topLeft_y >= 0 && swamp.Item2.Equals(topLeft_x))
                    return 0;
                else if (bottomRight_y - swamp.Item1 >= 0 && swamp.Item1 - topLeft_y >= 0 && swamp.Item2.Equals(bottomRight_x))
                    return 0;
            }
            return 2 * (bottomRight_y - topLeft_y) + 2 * (bottomRight_x - topLeft_x);
        }
    }
}
