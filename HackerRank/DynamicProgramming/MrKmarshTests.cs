﻿using System;
using DynamicProgramming;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;

namespace DynamicProgrammingTests
{
    public class MrKmarshTests
    {
        static void Main(String[] args)
        {
            string resource_data = DynamicProgramming.Properties.Resources.MrKmarshTestCases;
            string[] lines = resource_data.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            var queue = new Queue<string>(lines);
            while(queue.Any())
            {
                var dimensions = queue.Dequeue().Split(' ');
                var height = Convert.ToInt32(dimensions[0]);
                var width = Convert.ToInt32(dimensions[1]);
                var cases = new char[height][];
                var largestPeremeter = 0;
                for (int i = 0; i < height; i++)
                {
                    cases[i] = queue.Dequeue().ToArray();
                }
                largestPeremeter = MrKmarsh.GetLargestPeremeter(height, width, cases);
                var correctResult = queue.Dequeue();

                if (largestPeremeter != 0)
                    Console.WriteLine(correctResult.Equals(largestPeremeter.ToString()));
                else
                    Console.WriteLine(correctResult.Equals("impossible"));
            }
            Console.ReadLine();
        }
    }
}
