﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicProgramming
{
    class TheMaximumSubarray
    {
        static void Main(String[] args)
        {
            var numCases = Convert.ToInt32(Console.ReadLine());
            var cases = new int[numCases][];
            for(int caseIndex = 0; caseIndex < numCases; caseIndex++)
            {
                var numElements = Console.ReadLine();
                string[] tokens = Console.ReadLine().Split(' ');
                cases[caseIndex] = Array.ConvertAll(tokens, Int32.Parse);
            }
            foreach(var currentCase in cases)
            {
                Console.WriteLine(MaxSum(currentCase) + " " + MaxSumNonContiguous(currentCase));
            }
        }

        public static int MaxSum(int [] array)
        {
            var currentMax = array[0];
            var globalMax = currentMax;

            for (int index = 1; index < array.Length; index++)
            {
                currentMax = Math.Max(array[index], currentMax + array[index]);
                globalMax = Math.Max(globalMax, currentMax);
            }
            return globalMax;
        }

        public static int MaxSumNonContiguous(int[] array)
        {
            int? sum = null;
            int greatestNumber = array[0];
            foreach(var index in array)
            {
                if (index > 0)
                {
                    if (sum == null)
                        sum = index;
                    else
                        sum += index;

                }
                else if (index > greatestNumber)
                    greatestNumber = index;
            }
            if (sum == null)
                return greatestNumber;
            else
                return sum.Value;
        }
    }
}
