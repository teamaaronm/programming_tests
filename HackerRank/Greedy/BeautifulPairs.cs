﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Greedy
{
    class BeautifulPairs
    {
        static void Main(String[] args)
        {
            var numElements = Convert.ToInt32(Console.ReadLine());
            var a = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            var b = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            var matchedA = new int[numElements];
            var matchedB = new int[numElements];
            var numPairs = 0;
            for (int i = 0; i < numElements; i++)
            {
                for (int j = 0; j < numElements; j++)
                {
                    if (matchedA[i].Equals(0) && matchedB[j].Equals(0) && a[i].Equals(b[j]))
                    {
                        matchedA[i] = 1;
                        matchedB[j] = 1;
                        numPairs++;
                    }
                }
            }
            var changes = 1;
            for (int i = 0; i < numElements; i++)
            {
                if (matchedA[i].Equals(0))
                {
                    for (int j = 0; j < numElements; j++)
                    {
                        if (matchedA[j].Equals(0))
                        {
                            if ((a[i].Equals(b[i])))
                            {
                                numPairs++;
                            }
                            else if (changes > 0)
                            {
                                numPairs++;
                                changes--;
                                matchedA[i] = 1;
                                matchedB[j] = 1;
                            }
                        }
                    }
                }
            }
            if (changes > 0)
                numPairs--;
            Console.WriteLine(numPairs);
            Console.ReadKey();
        }
    }
}
