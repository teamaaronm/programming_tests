﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmup
{
    class AVeryBigSum
    {
        static void Main(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            string[] arr_temp = Console.ReadLine().Split(' ');
            Int64[] arr = Array.ConvertAll(arr_temp, Int64.Parse);
            Console.WriteLine(arr.Sum());
        }
    }

}
