﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmup
{
    class DiagonalDifference
    {
        static void Main(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int[][] a = new int[n][];
            for (int a_i = 0; a_i < n; a_i++)
            {
                string[] a_temp = Console.ReadLine().Split(' ');
                a[a_i] = Array.ConvertAll(a_temp, Int32.Parse);
            }
            var primaryDiagonal = 0;
            var secondaryDiagonal = 0;
            for(int primary_i = 0; primary_i < n; primary_i++)
            {
                primaryDiagonal += a[primary_i][primary_i];
            }
            for (int secondary_i = n - 1; secondary_i >= 0; secondary_i--)
            {
                secondaryDiagonal += a[secondary_i][(n - 1) - secondary_i];
            }
            Console.WriteLine(Math.Abs(secondaryDiagonal - primaryDiagonal));
        }
    }
}
