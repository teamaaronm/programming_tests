﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmup
{
    class PlusMinus
    {
        static void Main(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            string[] arr_temp = Console.ReadLine().Split(' ');
            int[] arr = Array.ConvertAll(arr_temp, Int32.Parse);
            var counts = new Dictionary<string, int>()
            {
                { "negative",  0 },
                { "positive", 0 },
                { "zeroes", 0 }
            };
            foreach (var number in arr)
            {
                if (number < 0)
                    counts["negative"]++;
                else if (number > 0)
                    counts["positive"]++;
                else
                    counts["zeroes"]++;
            }
            Console.WriteLine((decimal)counts["positive"] / (decimal)n);
            Console.WriteLine((decimal)counts["negative"] / (decimal)n);
            Console.WriteLine((decimal)counts["zeroes"] / (decimal)n);
        }
    }
}
