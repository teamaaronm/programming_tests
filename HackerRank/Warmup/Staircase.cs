﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmup
{
    class Staircase
    {
        static void Main(String[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            var stringBuilder = new StringBuilder();
            for(int height = 0; height < n; height++)
            {
                var numSpaces = (n-1) - height;
                var numSymbols = height + 1;
                for(int symbol = 0; symbol < numSpaces; symbol++)
                {
                    stringBuilder.Append(" ");
                }
                for (int symbol = 0; symbol < numSymbols; symbol++)
                {
                    stringBuilder.Append("#");
                }
                Console.WriteLine(stringBuilder.ToString());
                stringBuilder.Clear();
            }
        }
    }
}
