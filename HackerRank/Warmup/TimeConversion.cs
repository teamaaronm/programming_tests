﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmup
{
    class TimeConversion
    {
        static void Main(String[] args)
        {
            string time = Console.ReadLine();
            var hour = Convert.ToInt32(time.Substring(0, 2));
            var minute = time.Substring(3, 2);
            var second = time.Substring(6, 2);
            var isAM = time.Substring(8, 2).Equals("AM");
            if (!isAM)
            {
                if(hour != 12)
                    hour += 12;
            }
            else
            {
                if (hour == 12)
                    hour = 0;
            }

            Console.WriteLine(string.Format("{0:D2}:{1}:{2}", hour, minute, second));
        }
    }
}
