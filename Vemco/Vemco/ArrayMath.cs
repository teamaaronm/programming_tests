﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vemco
{
    class ArrayMath
    {
        /// <summary>
        /// Calculate the weighted mean given a list of values and weights.
        /// </summary>
        /// <param name="values"></param>
        /// <param name="weights"></param>
        /// <returns>Mean</returns>
        public static double WeightedMean(IList<int> values, IList<int> weights)
        {
            if (values == null)
                throw new ArgumentException("Input values cannot be null.");
            else if (weights == null)
                throw new ArgumentException("Input weights cannot be null.");
            else if (!values.Count.Equals(weights.Count))
                throw new ArgumentException("Input values and weights must have an equal count.");
            int sum = 0;
            int sumWeights = 0;
            for (int i = 0; i < values.Count; i++)
            {
                sum += values[i] * weights[i];
                sumWeights += weights[i];
            }

            return sum / sumWeights;
        }

        public static void Main(string[] args)
        {
            int[] values = new int[] { 0, Int32.MinValue };
            int[] weights = new int[] { Int32.MaxValue, -1 };

            Console.WriteLine(ArrayMath.WeightedMean(values, weights));
        }
    }
}
