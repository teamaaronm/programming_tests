﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Vemco
//{
//    public class Permissions
//    {
//        /// <summary>
//        /// Mapping of characters to values.
//        /// </summary>
//        static Dictionary<char, int> charactersToValues = new Dictionary<char, int>()
//        {
//            { 'r', 4 },
//            { 'w', 2 },
//            { 'x', 1 },
//            { '-', 0 }
//        };

//        /// <summary>
//        /// Convert a permission string to an int.
//        /// </summary>
//        /// <param name="permString"></param>
//        /// <returns>The resulting int.</returns>
//        public static int PermStringToInt(string permString)
//        {
//            var result = new StringBuilder();
//            //For each block map the characters to numbers and add them together. Append the results to result.
//            for(int index = 0; index < 3; index++)
//            {
//                var characters = permString.Substring(index * 3, 3);
//                var sum = 0;
//                foreach(var character in characters)
//                {
//                    sum += charactersToValues[character];
//                }
//                result.Append(sum);
//            }
//            return Convert.ToInt32(result.ToString());
//        }

//        public static void Main(string[] args)
//        {
//            // Should write 752
//            Console.WriteLine(Permissions.PermStringToInt("rwxr-x-w-"));
//        }
//    }
//}
