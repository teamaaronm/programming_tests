﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Vemco
//{
//    public class Schedule
//    {
//        /// <summary>
//        /// Using a greedy algorithm find the optimal schedule to maximize the amount of shows with no overlaps.
//        /// </summary>
//        /// <param name="shows"></param>
//        /// <returns></returns>
//        public static IList<Show> FindOptimalSchedule(ICollection<Show> shows)
//        {
//            var results = new List<Show>();
//            var sortedShows = new SortedSet<Show>(shows, new ByShowEnd());
//            while(sortedShows.Any())
//            {
//                var showEarliestEndTime = sortedShows.First();
//                results.Add(showEarliestEndTime);
//                sortedShows.Remove(showEarliestEndTime);
//                //Remove intersecting shows.
//                while(sortedShows.Any() 
//                    && showEarliestEndTime.StartTime < sortedShows.First().EndTime && 
//                    sortedShows.First().StartTime < showEarliestEndTime.EndTime)
//                {
//                    sortedShows.Remove(sortedShows.First());
//                }
//            }
//            return results;
//        }

//        public static void Main(string[] args)
//        {
//            var format = System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat;

//            var shows = new List<Show>();
//            shows.Add(new Show("Show1", DateTime.Parse("8/6/2013 18:00", format), DateTime.Parse("8/6/2013 20:00", format)));
//            shows.Add(new Show("Show2", DateTime.Parse("8/6/2013 19:00", format), DateTime.Parse("8/6/2013 22:00", format)));
//            shows.Add(new Show("Show3", DateTime.Parse("8/6/2013 21:00", format), DateTime.Parse("8/6/2013 23:00", format)));
//            shows.Add(new Show("Show4", DateTime.Parse("8/5/2013 21:00", format), DateTime.Parse("8/7/2013 23:00", format)));
//            shows.Add(new Show("Show5", DateTime.Parse("8/6/2013 13:00", format), DateTime.Parse("8/6/2013 21:00", format)));
//            shows.Add(new Show("Show6", DateTime.Parse("8/6/2013 05:00", format), DateTime.Parse("8/6/2013 11:00", format)));
//            shows.Add(new Show("Show6", DateTime.Parse("8/11/2013 05:00", format), DateTime.Parse("8/11/2013 11:00", format)));
//            shows.Add(new Show("Show6", DateTime.Parse("8/11/2013 12:00", format), DateTime.Parse("8/11/2013 15:00", format)));
//            shows.Add(new Show("Show6", DateTime.Parse("8/11/2013 05:00", format), DateTime.Parse("8/11/2013 11:00", format)));
//            shows.Add(new Show("Show6", DateTime.Parse("8/11/2013 05:00", format), DateTime.Parse("8/11/2013 11:00", format)));

//            var list = FindOptimalSchedule(shows);
//            foreach (var show in list)
//            {
//                Console.WriteLine(show.Name);
//            }
//        }
//    }

//    public class Show
//    {
//        public string Name { get; private set; }

//        public DateTime StartTime { get; private set; }

//        public DateTime EndTime { get; private set; }

//        public Show(string name, DateTime startTime, DateTime endTime)
//        {
//            this.Name = name;
//            this.StartTime = startTime;
//            this.EndTime = endTime;
//        }
//    }

//    /// <summary>
//    /// Compare shows by EndTime
//    /// </summary>
//    public class ByShowEnd : IComparer<Show>
//    {
//        public int Compare(Show x, Show y)
//        {
//            return x.EndTime.CompareTo(y.EndTime);
//        }
//    }
//}
